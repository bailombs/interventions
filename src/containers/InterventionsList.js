

import ButtonCreate from '../components/ButtonCreate';
import Table from '../components/Table';

const InterventionsList = () => {

  return (
    <div className="interventions">
      <ButtonCreate />
      <Table />
    </div>
  );
}

export default InterventionsList;


