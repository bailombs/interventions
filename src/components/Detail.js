import { NavLink, useParams } from 'react-router-dom';
import { useGetInterventions } from '../services';

const Detail = () => {

    const params = useParams()
    const { interventions } = useGetInterventions()

    const paramsId = parseInt(params.id, 10)

    const intervention = interventions?.find((intervention) => intervention.id === paramsId)

    return (
        <section className='detail'>
            <div className='detail__btn'>
                <NavLink to='/' className='detail__btn--link'>Retour</NavLink>
            </div>
            <div className='detail__wrapper'>
                <li className='detail__wrapper--name'>{intervention?.name}</li>
                <li className="detail__wrapper--title">DESCRIPTION</li>
                <li className="detail__wrapper--descritption">{intervention?.description}</li>
                <li className="detail__wrapper--title">DEMANDEUR</li>
                <li className="detail__wrapper--sender">{intervention?.senderName}</li>
                <li>{intervention?.senderEmail}</li>
                <li>{intervention?.senderPhone}</li>
            </div>
        </section>
    );
}


export default Detail;