import React from 'react';
import { Link } from 'react-router-dom';
import { useGetInterventions } from '../services';

const ButtonCreate = () => {

    const { interventions } = useGetInterventions()
    const numberOfIntervention = interventions?.length

    return (
        <ul className='interventions__create'>
            <Link to='intervention/create' className='interventions__create--intervention'>
                <li> Créer une intervention</li>
            </Link>
            <li className='interventions__create--length'>{`${numberOfIntervention} interventions`}</li>
        </ul>
    );
};

export default ButtonCreate;