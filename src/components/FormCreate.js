import React, { useEffect, useState } from 'react';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import { createIntervention, updatIntervention, useGetInterventions } from '../services';


const FormCreate = ({ actionType }) => {
    const { interventions } = useGetInterventions()

    const params = useParams()
    const interventionId = parseInt(params?.id, 10)

    const [interventionData, setInterventionData] = useState(
        {
            name: "",
            description: "",
            senderName: "",
            senderEmail: "",
            senderPhone: ""
        }
    )

    const interventionModifying = interventions?.find((intervention) => intervention?.id === interventionId)


    const action = actionType === "creating" ? true : false

    const navigate = useNavigate()

    const { mutate } = useGetInterventions()

    useEffect(() => {

    }, [interventionData.name, interventionData.description, interventionData.senderEmail, interventionData.senderName, interventionData.senderPhone])

    let formatMailRegex = /^[a-z0-9.-_]+[@]{1}[a-z.-_]+[.]{1}[a-z]{2,8}$/

    // check all fields are filled
    let validIntervention = (interventionData.name?.trim() !== "") &&
        (interventionData.description?.trim() !== "") && (interventionData.senderName.trim() !== "") && (formatMailRegex.test(interventionData?.senderEmail?.trim()) === true) === true


    let validModifyingIntervention = (interventionModifying?.name?.trim() !== "") &&
        (interventionModifying?.description?.trim() !== "") && (interventionModifying?.senderName.trim() !== "") && (formatMailRegex.test(interventionModifying?.SenderEmail?.trim()) === true) === true

    console.log(interventionData)

    const valid = action ? validIntervention : validModifyingIntervention

    // fuction create intervention
    const handleCreate = async (e) => {
        e.preventDefault()

        try {
            const response = await createIntervention(interventionData)
            if (response.statusText) {
                mutate()
                navigate("/")
            }
        } catch (error) {
            console.log(error)
        }
    }


    const handleModifying = async (e) => {
        e.preventDefault()
        try {
            const response = await updatIntervention(interventionModifying?.id, {
                name: interventionData.name || interventionModifying?.name,
                description: interventionData.description || interventionModifying?.description,
                senderName: interventionData.senderName || interventionModifying?.senderName,
                senderEmail: interventionData.senderEmail || interventionModifying?.SenderEmail,
                senderPhone: interventionData.senderPhone || interventionModifying?.senderPhone
            })
            if (response.statusText) {
                mutate()
                navigate("/")
            }

        } catch (error) {
            console.log(error)
        }
    }

    // style color submit valid
    const styleValid = {
        backgroundColor: "#ffc107",
        color: "#fff"
    }

    return (
        <section className='register'>
            <form className='register__form' onSubmit={(e) => { action ? handleCreate(e) : handleModifying(e) }} >

                <div className='register__form--buttons'>
                    <button className='cancel'>
                        <NavLink to='/' className="cancel__link">Annuler</NavLink>
                    </button>
                    <input type="submit"
                        value={action ? "Creer" : "Modifier"}
                        className='valid'
                        style={valid ? styleValid : null}
                        disabled={valid ? false : true}
                    />
                </div>

                <div className='register__form--inptgroup'>
                    <label htmlFor='name'>NOM DE L'INTERVENTION</label>
                    <input
                        type="text" id='name'
                        onChange={(e) => { setInterventionData({ ...interventionData, [e.target.id]: e.target.value }) }}
                        defaultValue={interventionData.name || interventionModifying?.name}
                        autoComplete="off"
                        placeholder='Nom'
                    />
                </div>
                <div className='register__form--inptgroup'>
                    <label htmlFor='description'>DESCRIPTION</label>
                    <textarea
                        type="text" id='description'
                        onChange={(e) => { setInterventionData({ ...interventionData, [e.target.id]: e.target.value }) }}
                        defaultValue={interventionData.description || interventionModifying?.description}
                        autoComplete="off"
                        placeholder="Saissez la descritption de l'intervention"
                    />

                </div>
                <div className='register__form--inptgroup'>
                    <label htmlFor='senderName'>DEMANDEUR</label>
                    <input
                        type="text" id='senderName'
                        onChange={(e) => { setInterventionData({ ...interventionData, [e.target.id]: e.target.value }) }}
                        defaultValue={interventionData.senderName || interventionModifying?.senderName}
                        autoComplete="off"
                        placeholder=' Prénom Nom'
                    />
                </div>
                <div className='register__form--inptgroup'>
                    <label htmlFor='senderEmail'>EMAIL</label>
                    <input
                        type="text" id='senderEmail'
                        onChange={(e) => { setInterventionData({ ...interventionData, [e.target.id]: e.target.value }) }}
                        defaultValue={interventionData.senderEmail || interventionModifying?.SenderEmail}
                        autoComplete="off"
                        placeholder='email@domaine.fr'
                    />
                </div>
                <div className='register__form--inptgroup'>
                    <label htmlFor='senderPhone'>TELEPHONE</label>
                    <input
                        type="text" id='senderPhone'
                        onChange={(e) => { setInterventionData({ ...interventionData, [e.target.id]: e.target.value }) }}
                        defaultValue={interventionData.senderPhone || interventionModifying?.senderPhone}
                        autoComplete="off"
                        placeholder='00 00 00 00 00'
                    />
                </div>
            </form>
        </section>
    );
};

export default FormCreate;