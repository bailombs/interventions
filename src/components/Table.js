import { IoMdArrowDropdown, IoMdArrowDropup } from 'react-icons/io';

import Row from './Row';
import { useGetInterventions } from '../services';




const Table = () => {

    const { interventions } = useGetInterventions()

    console.log(interventions);
    // const [sorted, setSorted] = useState(false)

    // const dateToString = (date) => {
    //     let dateFormat = new Date(date).toLocaleDateString('fr-FR', { day: "numeric", month: "short" }).split(' ')
    //     return dateFormat
    // }


    // const sortByDate = () => {

    //     const interventionsCopy = [...interventions];

    //     return interventionsCopy?.sort((a, b) => {
    //         let firstDate = a.created_at?.split(' ')[0]
    //         let secondDate = b.created_at?.split(' ')[0]
    //         if (firstDate > secondDate) {
    //             return 1;
    //         }
    //         if (firstDate < secondDate) {
    //             return -1;
    //         }
    //         return 0;
    //     })
    // }


    return (
        <section className='container'>
            <table className='table'>
                <thead className='table__head'>
                    <tr className='table__head--item'>
                        <th>
                            <div>
                                <span>DATE</span>
                                <div>
                                    <IoMdArrowDropup />
                                    <IoMdArrowDropdown />
                                </div>
                            </div>
                        </th>
                        <th>NOM</th>
                        <th>DESCRIPTION</th>
                        <th>DEMANDEUR</th>
                        <th>COORDONNEES</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody className='table__body'>
                    {interventions?.map((intervention) => (
                        <Row intervention={intervention} key={intervention.id} />
                    ))}
                </tbody>
            </table>

        </section >
    );
};

export default Table;