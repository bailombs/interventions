import { useNavigate } from 'react-router-dom';
import { HiOutlinePencilAlt, HiOutlineTrash } from "react-icons/hi";
import { deleteIntervention, useGetInterventions } from '../services';

const Row = ({ intervention }) => {
    /**
     * Format date in french Date
     * @param {*} date Date
     * @returns  12/04/2020 14:53:09
     */
    const formatFrenchDate = (date) => {
        return new Date(date).toLocaleString('fr-FR');
    }

    /**
     * format month french in 3 letter et day numeric
     * @param {*} date month and day
     * @returns  date format: 12 Avr
     */
    const dateToString = (date) => {
        let dateFormat = new Date(date).toLocaleDateString('fr-FR', { day: "numeric", month: "short" }).split(' ')
        return dateFormat
    }
    const { mutate } = useGetInterventions()


    const handleDelete = async (e) => {
        e.stopPropagation();

        const response = await deleteIntervention(intervention?.id)
        if (response.statusText) {
            mutate();
        }
    }

    const navigate = useNavigate()

    return (
        <tr id="intervention" className='table__body--item' onClick={() => navigate(`/intervention/${intervention?.id}`)} >
            <td width="2.5em">
                <div>{dateToString(intervention?.createdAt)[0]} <br />{dateToString(intervention?.createdAt)[1]}</div>
            </td>
            <td width="20%"> <span className='name'>{intervention?.name}</span> <br /> <span className='time'>{formatFrenchDate(intervention?.createdAt)}</span></td>
            <td width="30%" >{intervention?.description}</td>
            <td width="17%">{intervention?.senderName}</td>
            <td>{intervention?.SenderEmail} <br /> {intervention?.senderPhone}</td>
            <td width="8%">
                <HiOutlinePencilAlt className='modify' onClick={(e) => { e.stopPropagation(); navigate(`/intervention/modify/${intervention.id}`) }} />
                <HiOutlineTrash className='delete' onClick={(e) => {
                    if (window.confirm(`Voulez-vous supprimer l'intervention ${intervention?.name}`)) { handleDelete(e) }
                }} />
            </td>
        </tr>
    );
};

export default Row;