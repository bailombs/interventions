import { createSlice } from "@reduxjs/toolkit";

export const slice = createSlice({
    name: "interventions",
    initialState: {
        interventions: [],
    },
    reducers: {
        getInterventions: (state, action) => {
            state.interventions = action.payload;
        },
        createIntervention: (state, action) => {
            state.interventions.push(action.payload);
        }
    }
})

export const { getInterventions, createIntervention } = slice.actions;
export default slice.reducer;