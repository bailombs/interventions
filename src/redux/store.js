import { configureStore } from "@reduxjs/toolkit";
import interventionsReducer from './slicer';

export default configureStore({
    reducer: {
        interventions: interventionsReducer
    },

})
