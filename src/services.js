import axios from "axios";
import useSWR from "swr";

const axiosPrivate = axios.create({
    baseURL: "https://api-interventions-af357e37e47e.herokuapp.com/api",
    headers: {
        "Content-Type": "application/json"
    },
})


export const useGetInterventions = () => {
    const interventions = async (url) => {
        return await axiosPrivate.get(url).then((res) => res.data)
    }

    const { data, isLoading, error, mutate } = useSWR("/interventions", (url) => interventions(url))

    return {
        interventions: data,
        isLoading: isLoading,
        isError: error,
        mutate
    }
}


export const createIntervention = (postData) => {
    const response = axiosPrivate.post("/interventions", postData);
    return response;
};

export const updatIntervention = (id, postData) => {
    const response = axiosPrivate.put(`/interventions/${id}`, postData);
    return response
}

export const deleteIntervention = (id) => {
    const response = axiosPrivate.delete(`/interventions/${id}`)
    return response
}