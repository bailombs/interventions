import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Detail from './components/Detail';
import FormCreate from './components/FormCreate';
import Header from './components/Header';
import InterventionsList from './containers/InterventionsList';
import './styles/index.scss';


const App = () => {
  return (
    <div className="container">
      <Header />
      <Routes>
        <Route path='/' element={<InterventionsList />} />
        <Route path='intervention/create' element={<FormCreate actionType="creating" />} />
        <Route path='/intervention/:id' element={<Detail />} />
        <Route path='/intervention/modify/:id' element={<FormCreate actionType="modifying" />} />
      </Routes>
    </div>
  );
};

export default App;


