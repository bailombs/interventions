# Nautilux Frontend Test

This application is front web react application responsive for Interventions, Create intervention, display intervention from th store redux , get all interventions from to the data base send it to the store.
This application was developed React mongoose, Reactjs, redux-toolkit and sass nodejs express.

Its use an API RESTful make with Php8 and Symfony6 deploy an heroku CLOUD APPLICATION PLATFORM

## Prerequisites

- [Node.js (v16.0.0) +](https://nodejs.org/en/)
- [Recommended IDE (Visual Studio Code)](https://code.visualstudio.com)

## Installation

First of all, clone the project with HTTPS or Clone the repo onto your computer

```bash
  git clone https://gitlab.com/bailombs/interventions
```

The second time you need to install dependencies for the Back_end and the Front_end

> Le champ adresse mail est controlé par un regex, l'email doit respecter le format: email@domaine.

```


```

## Frontend

```
yarn start or npm start
```

## Dependencies

| Name             |
| ---------------- |
| react            |
| react-router-dom |
| react-scripts    |
| react-icons      |
| react-redux      |
| redux toolkit    |
| axios            |
| sass             |
| react-icons      |
